let height,
    width,
    bomb,
    bombMatrix;

document.addEventListener('click', function (evt) {
    if (evt.target.nodeName.toLowerCase() === 'div') {
        processButtonClick(evt.target);
    }
});

document.addEventListener('contextmenu', function (evt) {
    if (evt.target.nodeName.toLowerCase() === 'div') {
        evt.preventDefault();
        processButtonClickRight(evt.target);
    }
});

document.getElementById('start').addEventListener('click', (e) => matrixData(e))
document.getElementById('restart').addEventListener('click', reload)

function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function obj() {
    let self = this;
    this.positionId;
    this.bomb = false;
    this.open = false;
    this.iThink = false;
    this.elem = null;
    this.create = function (a, b) {
        let newDIV = document.createElement("div");
        let newP = document.createElement("p");
        newDIV.classList.add("cell");
        newDIV.id = "m" + a + "." + b;
        let table = document.getElementById('table');
        table.appendChild(newDIV);
        if ((b + 1) % (height + 1) === 0) {
            table.appendChild(document.createElement("br"));
        }
        self.elem = newDIV;
    };
}


function matrix() {
    let matrix = [];
    for (let i = 0; i <= width; i++) {
        matrix[i] = [];
        for (let j = 0; j <= height; j++) {
            matrix[i][j] = new obj();
            matrix[i][j].create(i, j);
        }
    }
    return matrix;
}

function processButtonClick(element) {
    element.classList.add("near");
    let takeIJs = element.id.slice(1, element.id.length).split(".");
    let i = +takeIJs[0];
    let j = +takeIJs[1];
    if (bombMatrix[i][j].iThink) {
        bombMatrix[i][j].iThink = false;
        element.classList.remove("flag");
    }
    if (bombMatrix[i][j].bomb) {
        element.classList.add("bomb");
        document.getElementById('table').classList.add('no-click')
        alert("GAME OVER");
        // reload();
    } else {
        if (bombAround(i, j) === 0) {
            element.classList.add("empty");
            let nearestCells = getSurroundCells(i, j);
            nearestCells.forEach(openCell);
        }
        element.innerHTML = "<p>" + bombAround(i, j) + "</p>";
    }
    bombMatrix[i][j].open = true;
}


function openCell(item) {
    if (!item.open) {
        item.elem.click();
    }
}

function processButtonClickRight(element) {
    let takeIJs = element.id.slice(1, element.id.length).split(".");
    let i = +takeIJs[0];
    let j = +takeIJs[1];
    if (!bombMatrix[i][j].open) {
        if (!bombMatrix[i][j].iThink) {
            bombMatrix[i][j].iThink = true;
            element.classList.add("flag");
        } else {
            bombMatrix[i][j].iThink = false;
            element.classList.remove("flag");
        }
    }
    if (isBombPositionTrue()) {
        alert('YOU WIN');
        // reload();
    }
}

function bombPosition() {
    for (let b = 0; b < bomb; b++) {
        function bombOne() {
            let i = random(0, height);
            let j = random(0, width);
            if (bombMatrix[i][j].bomb) {
                bombOne();
            } else {
                bombMatrix[i][j].bomb = true;
            }
        }
        bombOne();
    }
}


function isBombPositionTrue() {
    let arr = bombMatrix.reduce(function (a, b) {
        return a.concat(b);
    });
    let sumBombFlaf = arr.filter(function (a) {
        return a.iThink;
    });
    if (bomb === sumBombFlaf.length) {
        let bombArr = arr.filter(function (a) {
            return a.bomb;
        });
        return bombArr.every(function (a) {
            return a.bomb === a.iThink;
        });
    }
}

function getSurroundCells(i, j) {
    let arr = [];
    for (let a = i - 1; a <= i + 1; a++) {
        for (let b = j - 1; b <= j + 1; b++) {
            if (a >= 0 && a <= width && b >= 0 && b <= height) {
                if (a === i && b === j) { } else {
                    arr.push(bombMatrix[a][b]);
                }
            }
        }
    }
    return arr;
}

function bombAround(i, j) {
    return getSurroundCells(i, j).filter(function (elem) {
        return elem.bomb == true;
    }).length;
}

function reload() {
    location.reload(false);
}


function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function matrixData(e) {
    height = +document.getElementById("matrixSize").value - 1;
    width = +document.getElementById("matrixSize").value - 1;
    bomb = +document.getElementById("matrixBomb").value;
    

    e.target.classList.add('unvisible')
    document.getElementById('restart').classList.remove('unvisible')

    if (!isNumeric(height) || !isNumeric(bomb)) {
        alert("Ви ввели не число");
        reload();
    } else if (height === -1 || width === -1 || bomb === 0){
        alert('Введіть розмір поля та кількість бомб')
        reload()
    } else {
        if ((height + 1) * (width + 1) >= bomb) {
            bombMatrix = matrix();
            bombPosition();
        } else {
            alert("Бомб не може бути більше кількосіті комірок");
            reload();
        }
    }
}

